'use strict';

const twitterService = require('./service');

const username = process.env.USERNAME || 'brendaneich';
const limit = process.env.LIMIT || 10;
const iconSize = process.env.ICONSIZE || 50;

twitterService(username, limit, iconSize);
