# DataArt Workshop 10.12.2016 #

Create service that receives a twitter user name and returns a collage of friends and followers profile icons. Image should be available  in cloud by url.

### Instalation ###

Firstly, install LTS version of node.js https://nodejs.org/en/download/

In root directory please run command:

    npm install
    
I use imagemagick for image processing https://www.imagemagick.org/script/index.php

Actually, if you use Ubuntu you already have it: 

For Mac you can install with brew:

    brew install imagemagick

As for Windows i hope you could find instruction on imagemagick web site.

### Usage ###

You need to run application in console

    USERNAME=brendaneich LIMIT=50 ICONSIZE=50 node app.js
    
And you can see result  in you  terminal:
    
    Service starting...
     > Getting follower and friends list by brendaneich
    URL [https://api.twitter.com/1.1/friends/list.json?screen_name=brendaneich&count=50]
    URL [https://api.twitter.com/1.1/followers/list.json?screen_name=brendaneich&count=50]
     > Creating collage with user profile icons
     > Uploading image to s3
     > Collage generated susseccfully:  https://da-workshop.s3.amazonaws.com/1481336347.png
    Service stoped.
