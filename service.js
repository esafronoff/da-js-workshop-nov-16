'use strict';

const clc = require('cli-color');

const logger = console.log;
const AWS = require('aws-sdk');
const gm = require('gm').subClass({ imageMagick : true });
const Twitter = require('twitter-node-client').Twitter;
const fs = require('fs');
const moment = require('moment');

const config = JSON.parse(fs.readFileSync('./config.json').toString());
const twitter = new Twitter(config.Twitter);

const handler = (username, limit, iconSize) => {
    logger(clc.blue('Service starting...'));
    logger(clc.blue(` > Getting follower and friends list by ${username}`));

    return Promise.all([
        promisify(twitter.getCustomApiCall, twitter)('/followers/list.json', { screen_name : username, count : limit }),
        promisify(twitter.getCustomApiCall, twitter)('/friends/list.json', { screen_name : username, count : limit })
    ])
        .then(res => {
            logger(clc.blue(' > Creating collage with user profile icons'));

            const followers = JSON.parse(res[0]).users;
            const friends = JSON.parse(res[1]).users;
            const users = followers.concat(friends);
            const profileUrls = users.map(u => u.profile_image_url);

            return createCollage(profileUrls, iconSize);
        })
        .then(buffer => {
            logger(clc.blue(' > Uploading image to s3'));
            return uploadCollage(buffer);
        })
        .then(s3res => {
            logger(clc.green(` > Collage generated susseccfully:  ${s3res.Location}`));
            logger(clc.blue('Service stoped.'));

            return s3res.Location;
        })
        .catch(err => {
            logger(clc.red(` > Service failed: ${err}`));

            throw Error(`Service failed: ${err}`);
        });
};

const promisify = function(originAsyncFunc, context) {
    return function(...args) {
        return new Promise((resolve, reject) => {
            const error = e => reject(e);
            const success = s => resolve(s);
            args.push(error, success);
            originAsyncFunc.apply(context, args);
        });
    };
};

const s3 = new AWS.S3(config.AWS);

const uploadCollage = buffer =>
    new Promise((resolve, reject) =>
        s3.upload({
            Bucket : 'da-workshop',
            Key : `${moment().unix()}.png`,
            ACL : 'public-read',
            Body : buffer
        }, (err, res) => {
            if (err) reject(err);
            resolve(res);
        })
    );

const createCollage = (imageList, iconSize) =>
    new Promise((resolve, reject) =>
         gm()
            .command('montage')
            .in(...imageList)
            .geometry(iconSize, iconSize, '+1', '+1')
            .toBuffer('png', (err, buffer) => {
                if (err) return reject(err);
                resolve(buffer);
            })
    );

module.exports = handler;
